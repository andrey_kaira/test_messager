import 'dart:convert';

import 'package:shelf/shelf.dart' as shelf;
import 'package:uuid/uuid.dart';

import '../models/auth_user.dart';
import '../models/user.dart';
import '../requests/local_store_request/auth_local_store_request.dart';
import '../res/functions_keys.dart';

class AuthFunctions {
  static Future<shelf.Response> registration(Map<String, String> params, Map<String, dynamic> body) async {
    final login = body[AuthKeys.login];
    final password = body[AuthKeys.password];

    if (login == null || login == '') {
      return shelf.Response.notFound('Login is Empty!');
    }
    if (password == null || password == '') {
      return shelf.Response.notFound('Password is Empty!');
    }

    final checkUser = await AuthLocalStoreRequest.isContainUserLogin(login);

    if (checkUser) {
      return shelf.Response.forbidden('This login is already registered!');
    }

    await AuthLocalStoreRequest.saveNewUser(
      User(id: await AuthLocalStoreRequest.getUserCount(), login: login, password: password),
    );

    final user = await AuthLocalStoreRequest.getUserByLogin(login);
    final authUser = AuthUser(
      user: user,
      token: Uuid().v4(),
    );

    await AuthLocalStoreRequest.saveNewAuthUser(authUser);

    return shelf.Response.ok(jsonEncode(authUser.toJson()));
  }

  static Future<shelf.Response> login(Map<String, String> params, Map<String, dynamic> body) async {
    final login = body[AuthKeys.login];
    final password = body[AuthKeys.password];

    if ((login == '' || login == null) && (password == '' || password == null)) {
      return shelf.Response.notFound('The fields are not filled in!');
    }
    if (login == '' || login == null) return shelf.Response.notFound('Email is Empty!');
    if (password == '' || password == null) return shelf.Response.notFound('Password is Empty!');

    final user = await AuthLocalStoreRequest.getUser(login, password);

    if (user == null) {
      return shelf.Response.notFound('Email or password entered incorrectly!');
    }
    final authUser = AuthUser(
      user: user,
      token: Uuid().v4(),
    );

    await AuthLocalStoreRequest.saveNewAuthUser(authUser);

    return shelf.Response.ok(jsonEncode(authUser.toJson()));
  }
}
