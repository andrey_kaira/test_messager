import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:args/args.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

import 'functions/auth_functions.dart';
import 'models/message.dart';
import 'res/key.dart';

// For Google Cloud Run, set _hostname to '0.0.0.0'.
//const _hostname = '192.241.137.110';
const _hostname = 'localhost';

ServerSocket serverSocket;

void main(List<String> args) async {
  print((await NetworkInterface.list()).first.addresses.first.address);
  var parser = ArgParser()..addOption('port', abbr: 'p');
  var result = parser.parse(args);

  // For Google Cloud Run, we respect the PORT environment variable
  var portStr = result['port'] ?? Platform.environment['PORT'] ?? '8081';
  var port = int.tryParse(portStr);

  if (port == null) {
    stdout.writeln('Could not parse port value "$portStr" into a number.');
    // 64: command line usage error
    exitCode = 64;
    return;
  }

  var handler = const shelf.Pipeline().addMiddleware(shelf.logRequests()).addHandler(_echoRequest);

  var server = await io.serve(handler, _hostname, port);

  print('Serving at http://${server.address.host}:${server.port}');

  serverSocket = await ServerSocket.bind(server.address.host, 4567);
  await serverSocket.listen((client) => handleConnection(client));
}

Future<shelf.Response> _echoRequest(shelf.Request request) async {
  final bodyMap = _getBody(await request.readAsString()) ?? {};
  switch (request.url.path) {
    case ApiKey.registration:
      return await AuthFunctions.registration(request.url.queryParameters, bodyMap);
    case ApiKey.login:
      return await AuthFunctions.login(request.url.queryParameters, bodyMap);
    default:
      return shelf.Response.notFound('Function not found!');
  }
}

Map<String, dynamic> _getBody(String body) {
  try {
    if (body == null || body == '') return null;
    var data = <MapEntry<String, dynamic>>[];
    var index = 1;
    const kSearchKey = 'Content-Disposition: form-data; name=';
    if (body.contains(kSearchKey)) {
      var encodeStr = jsonEncode(body);
      var clearStr = encodeStr.replaceAll('\\r', '').replaceAll('\\n', '');
      while (index != kSearchKey.length + 1) {
        index = clearStr.indexOf(kSearchKey, index) + kSearchKey.length + 2;
        if (index != kSearchKey.length + 1) {
          final key = clearStr.substring(index, clearStr.indexOf('\\"', index));
          final value = clearStr.substring(index + key.length + 2, clearStr.indexOf('------------------------', index)).replaceAll('\\"', '"');
          data.add(MapEntry(key, value));
        }
      }
    } else if (body.substring(0, 1) == '{') {
      return jsonDecode(body);
    } else {
      body = body.replaceAll('=', '" : "').replaceAll('&', '", "');
      body = '{"$body"}';
      return jsonDecode(body);
    }
    return Map.fromEntries(data);
  } catch (error) {
    print('Read body error');
    return null;
  }
}

List<Message> messages = [];
List<Socket> clients = [];

void handleConnection(Socket client) {
  clients.add(client);
  print('Connection from' ' ${client.remoteAddress.address}:${client.remotePort}');
  if (messages.isNotEmpty) {
    client.write(jsonEncode(messages
        .map(
          (e) => {
            'online': clients.length,
            'message': e.toJson(),
          },
        )
        .toList()));
  }

  client.listen(
    (Uint8List data) async {
      final message = Utf8Codec().decode(data);
      messages.add(Message.fromJson(jsonDecode(message)));
      clients.forEach((element) {
        element.write(
          jsonEncode({
            'online': clients.length,
            'message': message,
          }),
        );
      });
    },

    onError: (error) {
      print(error);
      clients.remove(client);
      client.close();
    },

    onDone: () {
      clients.remove(client);
      client.close();
    },
  );
}
