import 'dart:convert';
import 'dart:io';

import '../../models/auth_user.dart';
import '../../models/user.dart';


class AuthLocalStoreRequest {
  static Future<void> saveNewAuthUser(AuthUser user) async {
    var title = 'auth_users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      await file.writeAsString(jsonEncode([user.toJson()]));
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(AuthUser.fromJson(element));
      });
      users.add(user);
      await file.writeAsString(jsonEncode(users.map((e) => e.toJson()).toList()));
    }
  }

  static Future<void> saveNewUser(User user) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([user.toJson()]));
    } else {
      final users = <User>[];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      users.add(user);
      await file.writeAsString(jsonEncode(users.map((e) => e.toJson()).toList()));
    }
  }

  static Future<bool> isContainUserLogin(String login) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      return false;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.indexWhere((element) => element.login == login) != -1;
    }
  }

  static Future<User> getUser(String login, String password) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.firstWhere((element) => element.login == login && element.password == password, orElse: () => null);
    }
  }

  static Future<int> getUserCount() async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return 0;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.length;
    }
  }

  static Future<User> getUserByLogin(String login) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.firstWhere((element) => element.login == login, orElse: () => null);
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
