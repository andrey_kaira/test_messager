import 'user.dart';

class AuthUser {
  final User user;
  final String token;

  AuthUser({
    this.user,
    this.token,
  });

  @override
  String toString() {
    return '{user $user,token: $token}';
  }

  factory AuthUser.fromJson(dynamic map) {
    return AuthUser(
      token: map['token'],
      user: User.fromJson(map['user']),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'token': token,
    'user': user.toJson(),
  };
}
