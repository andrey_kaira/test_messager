import '../res/message_type.dart';

class Message {
  final int idUser;
  final String message;
  final String type;
  final DateTime dateTime;
  final int color;
  final String image;
  final String login;

  Message({
    this.idUser,
    this.message,
    this.type = MessageType.message,
    this.dateTime,
    this.color,
    this.image,
    this.login,
  }) {
    dateTime ?? DateTime.now();
  }

  factory Message.fromJson(dynamic map) {
    print(map);
    return Message(
      idUser: map['idUser'],
      message: map['message'].toString(),
      dateTime: map['dateTime'] != null ? DateTime.parse(map['dateTime']) : null,
      type: map['type'],
      color: map['color'],
      image: map['image'],
      login: map['login'],
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'idUser': idUser,
    'message': message,
    'dateTime': dateTime.toString(),
    'type': type,
    'color': color,
    'image': image,
    'login': login,
  };
}

