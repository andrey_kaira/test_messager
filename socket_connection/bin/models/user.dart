import '../requests/local_store_request/auth_local_store_request.dart';

class User {
  final int id;
  final String login;
  final String password;
  String image;
  int color;

  User({
    this.id,
    this.password,
    this.login,
    this.image,
    this.color,
  }) {
    color ??= _getColor(id);
    image ??= _getImage(id);
  }

  static int _count = 1;

  @override
  String toString() {
    return 'User{id: $id login: $login, password: $password, color: $color, image: $image}';
  }

  factory User.fromJson(dynamic map) {
    return User(
      id: map['id'],
      login: map['login'].toString(),
      password: map['password'].toString(),
      color: map['color'],
      image: map['image'].toString(),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'login': login,
        'color': color,
        'image': image,
        'password': password,
      };

  static int _getColor(int i) {
    switch (i % 10) {
      case 0:
        return 0XFF70FF80;
      case 1:
        return 0XFFFF8B70;
      case 2:
        return 0XFFFF70C6;
      case 3:
        return 0XFF78DBE2;
      case 4:
        return 0XFFE32636;
      case 5:
        return 0XFFF19CBB;
      case 6:
        return 0XFFF5F470;
      case 7:
        return 0XFF21834A;
      case 8:
        return 0XFFCCCCFF;
      case 9:
        return 0XFFFF43A4;
    }
    return 0XFF000000;
  }

  static String _getImage(int i) {
    switch (i % 11) {
      case 0:
        return 'https://upload.wikimedia.org/wikipedia/commons/8/8d/President_Barack_Obama.jpg';
      case 1:
        return 'https://doseng.org/uploads/posts/2009-02/1233623874_podborka_prikolnykh_zhivotnykh_20_foto_0.jpg';
      case 2:
        return 'https://i.ytimg.com/vi/h9IGiYQFbfw/maxresdefault.jpg';
      case 3:
        return 'https://st2.depositphotos.com/1036149/12479/i/950/depositphotos_124792324-stock-photo-pig-holding-cupcake.jpg';
      case 4:
        return 'https://www.sobaka.com/wp-content/uploads//anekdoty-pro-sobak.jpg';
      case 5:
        return 'https://telemetr.me/photos/d14b47baf70cdac3d396fbbcce05c003.jpg';
      case 6:
        return 'https://pbs.twimg.com/media/EeSMIk3XkAAYFr6.jpg';
      case 7:
        return 'https://i.pinimg.com/originals/9c/17/c1/9c17c11010a59e2fd89b304d492bb774.jpg';
      case 8:
        return 'https://stihi.ru/pics/2012/11/12/2259.jpg';
      case 9:
        return 'https://img.freepik.com/free-photo/funky-santa-on-red_236854-20971.jpg?size=626&ext=jpg';
      case 10:
        return 'https://apostrophe.ua/uploads/image/24a52002d0e23d8f21de4efe138c3b03.PNG';
    }
    return 'https://apostrophe.ua/uploads/image/24a52002d0e23d8f21de4efe138c3b03.PNG';
  }
}
